﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoWPF3.Entities.Models
{
    [Serializable]
    public class Contact : Person
    {
        public string Email { get; set; }
        public ICollection<string> Phones {get;set;}
    }
}
