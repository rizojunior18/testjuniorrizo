﻿using DemoWPF3.Entities.Interfaces;
using DemoWPF3.Tools.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace DemoWPF3.Entities.Models
{
    [Serializable]
    public class Group:IToFile, IFromFile<Group>
    {
        public string Name { get; set; }
        public int? Length { get { return Members?.Count; } }
        public ICollection<Person> Members { get; set; }

        public Group FromBinary(string filepath)
        {
            throw new NotImplementedException();
        }

        public Group FromJson(string filepath)
        {
            throw new NotImplementedException();
        }

        public Group FromXml(string filepath)
        {
            throw new NotImplementedException();
        }

        public void ToBinary(string filepath)
        {
            throw new NotImplementedException();
        }

        public void ToJson(string filepath)
        {
            JsonSerialization.WriteToJsonFile(filepath, this);
        }

        public void ToXml(string filepath)
        {
            throw new NotImplementedException();
        }
    }
}
